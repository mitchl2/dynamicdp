﻿using System;
using System.Configuration;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text;

using Microsoft.Azure;
using Microsoft.Azure.Management.StreamAnalytics;
using Microsoft.Azure.Management.StreamAnalytics.Models;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using JobConfigUtil;

/**
* Utilities for viewing and modifying chained Stream Analytics. As Stream 
* Analytics can be chained together, it's possible to form a data processing 
* graph (DP graph) in an abstract sense. These utilities enable 
* modifications to the graph as a whole.
*/
namespace DPNet {
    class Program {
        static StreamAnalyticsManagementClient client;
        static UserConfig userConfig;

        /**
        * Prints out the general usage of this tool.
        */
        private static void generalUsage() {
            StringBuilder b = new StringBuilder();
            b.Append("USAGE: DPNet.exe [connect | disconnect | view | create | config\n");
            b.Append("                  start | stop | help | -h]");
            b.Append("\n\n");
            b.Append("Utilities for viewing and modifying a graph of StreamAnalytics\n");
            b.Append("nodes. NOTE: In order to use this tool you must create an\n");
            b.Append("azure.json file in the same directory as this executable.\n");
            b.Append("See the readme file for an explanation of what this file\n");
            b.Append("should contain.\n");
            b.Append("\n");
            b.Append("-h, help      prints this usage message");
            Console.WriteLine(b.ToString());
        }

        /**
        * Prints out connect-job usage.
        */
        private static void connectJobsUsage() {
            StringBuilder b = new StringBuilder();
            b.Append("USAGE: DPNet.exe connect <SOURCE_JOB> <DESTINATION_JOB> ");
            b.Append("<EVENT_HUB>");
            b.Append("\n\n");
            b.Append("Connects the output of a source job to the input of ");
            b.Append("a destination job.");
            Console.WriteLine(b.ToString());
        }

        /**
        * Prints out disconnect-job usage.
        */
        private static void disconnectJobsUsage() {
            StringBuilder b = new StringBuilder();
            b.Append("USAGE: DPNet.exe disconnect <SOURCE_JOB> ");
            b.Append("<DESTINATION_JOB> <SOURCE_JOB_OUTPUT_NAME> ");
            b.Append("<DESTINATION_JOB_INPUT_NAME>");
            b.Append("\n\n");
            b.Append("Disconnects two jobs.");
            Console.WriteLine(b.ToString());
        }

        /**
        * Prints out view-graph usage.
        */
        private static void viewGraphUsage() {
            StringBuilder b = new StringBuilder();
            b.Append("USAGE: DPNet.exe view [<SOURCE_JOB> | --all] ");
            b.Append("\n\n");
            b.Append("Displays a sequence of connected StreamAnalytics ");
            b.Append("nodes for a single job or for all jobs.");
            Console.WriteLine(b.ToString());
        }

        /**
        * Prints out the create-job usage.
        */
        private static void viewCreateUsage() {
            StringBuilder b = new StringBuilder();
            b.Append("USAGE: DPNet.exe create [job | flow] <JOB_NAME1>[;<JOB_NAME2>;<JOB_NAME3>...]");
            b.Append("\n\n");
            b.Append("The job option will create new job(s). The flow ");
            b.Append("option will connect jobs in the order they are ");
            b.Append("specified.");
            Console.WriteLine(b.ToString());
        }

        /**
        * Prints out the configure-job usage.
        */
        private static void viewConfigureUsage() {
            StringBuilder b = new StringBuilder();
            b.Append("USAGE: DPNet.exe config <CONFIG_PATH> [-r] [-f] <JOB_NAME1>[;<JOB_NAME2>;<JOB_NAME3>...]");
            b.Append("\n\n");
            b.Append("The job option will configure job(s).\n");
            b.Append("\n");
            b.Append("-r    resets job config before applying the new config\n");
            b.Append("-f    will stop running jobs before applying the new config");
            Console.WriteLine(b.ToString());
        }

        /**
        * Prints out the start usage.
        */
        private static void viewStartUsage() {
            StringBuilder b = new StringBuilder();
            b.Append("USAGE: DPNet.exe start <JOB_NAME1>[;<JOB_NAME2>;<JOB_NAME3>...]");
            b.Append("\n\n");
            b.Append("Starts a job(s).");
            Console.WriteLine(b.ToString());
        }

        /**
        * Prints out the stop usage.
        */
        private static void viewStopUsage() {
            StringBuilder b = new StringBuilder();
            b.Append("USAGE: DPNet.exe stop <JOB_NAME1>[;<JOB_NAME2>;<JOB_NAME3>...]");
            b.Append("\n\n");
            b.Append("Stops a job(s).");
            Console.WriteLine(b.ToString());
        }

        /**
        * Initializes Azure credentials.
        */
        static void initCredentials() {
            // Get authentication token
            TokenCloudCredentials aadTokenCredentials = new TokenCloudCredentials(
                userConfig.SubscriptionId,
                GetAuthorizationHeader().Result);

            // Create Stream Analytics management client
            client = new StreamAnalyticsManagementClient(aadTokenCredentials);
        }

        /**
        * Parses command-line arguments.
        */
        static void parseArgs(string[] args) {
            if (args.Length == 0) {
                // invalid command
                generalUsage();
                Environment.Exit(1);
            } else {
                // check if the help option was specified
                if (args.Length == 1 
                 && (args[0].ToLower().Equals("-h")
                  || args[0].ToLower().Equals("help"))) {
                    // print help message
                    generalUsage();
                    return;
                }

                // load azure credentials
                string azureConfigPath = @"azure.json";

                if (!File.Exists(azureConfigPath)) {
                    throw new Exception(
                        "Expected to find Azure credentials file: " 
                       + azureConfigPath
                    );
                }
                
                try {
                    userConfig = JsonConvert.DeserializeObject<UserConfig>(
                        File.ReadAllText(azureConfigPath));
                } catch (Exception e) {
                    Console.Error.WriteLine(
                        "Failed to read config file: " + e.Message);
                    Environment.Exit(1);
                }

                // check that all properties were set as expected
                foreach (var prop in userConfig.GetType().GetProperties()) {
                    var value = prop.GetValue(userConfig);
                    if (value == null || value.ToString().Length == 0) {
                        throw new Exception(
                            "Missing property from Azure credentials file: " 
                           + prop.Name    
                        );
                    }
                }
                
                // check which command was invokved
                switch (args[0]) {
                    case "connect":
                        if (args.Length == 4) {
                            initCredentials();

                            DPClient p = new DPClient(client, userConfig);

                            p.connectJobs(args[1],
                                          args[2],
                                          args[3],
                                          userConfig.ServiceBusNamespace,
                                          userConfig.ResourcesGroupName,
                                          userConfig.SharedAccessPolicyName,
                                          userConfig.SharedAccessPolicyKey);
                        } else {
                            // invalid commands
                            connectJobsUsage();
                        }
                        break;
                    case "disconnect":
                        if (args.Length == 5) {
                            initCredentials();

                            DPClient p = new DPClient(client, userConfig);

                            p.disconnectJobs(args[1],
                                             args[2],
                                             args[3],
                                             args[4],
                                             userConfig.ResourcesGroupName);
                        } else {
                            // invalid command
                            disconnectJobsUsage();
                        }
                        break;
                    case "view":
                        if (args.Length == 2) {
                            initCredentials();

                            DPClient p = new DPClient(client, userConfig);

                            p.viewDPGraph(
                                args[1],
                                userConfig.ResourcesGroupName,
                                Array.FindIndex(args, x => x == "--all") != -1);
                        } else {
                            // invalid command
                            viewGraphUsage();
                        }
                        break;
                    case "create":
                        if (args.Length == 3) {
                            initCredentials();

                            DPClient p = new DPClient(client, userConfig);

                            if (args[1].Equals("job")) {
                                string[] jobNames = args[2].Split(';');

                                foreach (string jobName in jobNames) {
                                    p.createJob(jobName,
                                                userConfig.ResourcesGroupName,
                                                userConfig.DefaultLocation);
                                }
                            } else if (args[1].Equals("flow")) {
                                string[] jobNames = args[2].Split(';');

                                if (jobNames.Length <= 1) {
                                    Console.Error.WriteLine(
                                        "Expected more than 1 job name to construct a flow"
                                    );

                                    Environment.Exit(1);
                                } else {
                                    // create jobs if they do not already
                                    // exist
                                    p.createJobs(
                                        jobNames,
                                        userConfig.ResourcesGroupName,
                                        userConfig.DefaultLocation);

                                    // connect each job to its successor
                                    for (int i = 0; i <= jobNames.Length - 2; i++) {
                                        string srcJobName = jobNames[i];
                                        string destJobName = jobNames[i + 1];

                                        p.connectJobs(srcJobName,
                                                      destJobName,
                                                      userConfig.ServiceBusNamespace,
                                                      userConfig.ResourcesGroupName,
                                                      userConfig.SharedAccessPolicyName,
                                                      userConfig.SharedAccessPolicyKey);
                                    }
                                }
                            } else {
                                // invalid command
                                viewCreateUsage();
                            }
                        } else {
                            // invalid command
                            viewCreateUsage();
                        }
                        break;
                    case "config":
                        if (args.Length >= 3 && args.Length <= 5) {
                            initCredentials();

                            // extract command option
                            string configPath = args[1];
                            List<string> argList = new List<string>(args);

                            Boolean isResetConfigRequested
                                = argList.Contains("-r");

                            Boolean isForceRequested
                                = argList.Contains("-f");

                            string[] jobNames
                                = argList[argList.Count - 1].Split(';');

                            if (!File.Exists(args[1])) {
                                Console.Error.WriteLine(
                                    String.Format(
                                        "Config file {0} does not exist",
                                        args[2]
                                    )
                                );

                                Environment.Exit(1);
                            }
                            
                            foreach (string jobName in jobNames) {
                                ConfigureJob.Config(
                                    client,
                                    userConfig.ResourcesGroupName,
                                    jobName,
                                    configPath,
                                    isResetConfigRequested,
                                    isForceRequested);
                            }
                        } else {
                            // invalid command
                            viewConfigureUsage();
                        }
                        break;
                    case "start":
                        if (args.Length == 2) {
                            initCredentials();
                            DPClient p = new DPClient(client, userConfig);

                            foreach (string jobName in args[1].Split(';')) {
                                p.startJob(jobName,
                                           userConfig.ResourcesGroupName);
                            }
                        } else {
                            // invalid command
                            viewStartUsage();
                        }
                        break;
                    case "stop":
                        if (args.Length == 2) {
                            initCredentials();
                            DPClient p = new DPClient(client, userConfig);

                            foreach (string jobName in args[1].Split(';')) {
                                p.stopJob(jobName,
                                          userConfig.ResourcesGroupName);
                            }
                        } else {
                            // invalid command
                            viewStopUsage();
                        }
                        break;
                    default:
                        generalUsage();
                        Environment.Exit(1);
                        break;
                }
            }
        }

        static void Main(string[] args) {   
            try {
                parseArgs(args);
            } catch (Exception e) {
                Console.Error.WriteLine(
                    e.Message 
                 + ", see err.log for debugging information");

                string errLogName = "err.log";

                if (!File.Exists(errLogName)) {
                    File.Create(errLogName).Close();
                }

                File.WriteAllText(errLogName, e.ToString());
            }
        }

        // Helper method to get Azure credentials
        // See: https://docs.microsoft.com/en-us/azure/stream-analytics/stream-analytics-dotnet-management-sdk
        private static async Task<string> GetAuthorizationHeader() {
            var context = new AuthenticationContext(
                ConfigurationManager.AppSettings["ActiveDirectoryEndpoint"] +
                userConfig.ActiveDirectoryTenantId);

            AuthenticationResult result;

            // check if the user specified credentials in the config file
            if (userConfig.ClientId != null 
             && userConfig.ClientSecret != null) {
                result = await context.AcquireTokenAsync(
                    resource: ConfigurationManager.AppSettings["WindowsManagementUri"],
                    clientCredential: 
                        new ClientCredential(userConfig.ClientId,
                                             userConfig.ClientSecret)
                );                
            } else {
                // there are no pre-specified credentials
                result = await context.AcquireTokenAsync(
                    resource: ConfigurationManager.AppSettings["WindowsManagementUri"],
                    clientId: ConfigurationManager.AppSettings["AsaClientId"],
                    redirectUri: new Uri(ConfigurationManager.AppSettings["RedirectUri"]),
                    parameters: new PlatformParameters(PromptBehavior.Auto)
                );
            }
            
            if (result != null)
                return result.AccessToken;

            throw new InvalidOperationException("Failed to acquire token");
        }
    }
}                                                                              
                                                                               