﻿using DPNet;
using Microsoft.Azure.Management.StreamAnalytics;
using Microsoft.Azure.Management.StreamAnalytics.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

/**
* Contains classes and utilities for configuring a StreamAnalytics job
* based on a config file.
*/
namespace JobConfigUtil {
    /**
    * Exceptions that occur during job configuration.
    */
    class ConfigureJobException : Exception {
        public ConfigureJobException(String message) :base(message) {}
    }

    /**
    * Represents a job input or output configuration (ex: blob storage,
    * Azure table, etc.).
    */
    class JobIO {
        public string Type { get; set; }
        public string Name { get; set; }
        public Dictionary<string, string> Config { get; set; }

        /**
        * Returns the value for a configuration setting, checking if
        * the value has been set first.
        * 
        * @param configLookup A job input or output configuration.
        * @param configName Name of the configuration setting.
        */
        public string GetConfigValue(string configName, 
                                     Boolean isRequired=true) {
            if (!Config.ContainsKey(configName)) {
                if (!isRequired) {
                    return "";
                }

                throw new ConfigureJobException(
                    String.Format(
                        "Missing job configuration setting '{0}'",
                        configName)
                );
            }

            return Config[configName];
        }
    }

    /**
    * Subclasses of JobIO for inputs, outputs, and functions.
    */
    class InputConfig : JobIO {}
    class OutputConfig: JobIO {}
    class JobFunction : JobIO {}

    /**
    * Represents a parsed job config file.
    */
    class JobConfig {
        public List<InputConfig> Inputs { get; set; }
        public List<OutputConfig> Outputs { get; set; }
        public Dictionary<string, string> Transform { get; set; }
        public List<JobFunction> Functions { get; set; }
    }

    /**
    * Static utilities for configuring a StreamAnalytics job.
    */
    class ConfigureJob {
        // When setting the transform for a job we must give it a name, this
        // is the name we apply.
        public static string DEFAULT_TRANSFORM_NAME = "MyTransform";

        private StreamAnalyticsManagementClient client;
        private string resourcesGroupName;
        private string jobName;

        /**
        * Constructs a new ConfigureJob.
        * 
        * @param jobName The name of the job to configure.
        * @param client Manages Azure connections.
        * @param resourcesGroupName The name of the resources group for the
        * job.
        */
        public ConfigureJob(string jobName,
                            StreamAnalyticsManagementClient client, 
                            string resourcesGroupName) {
            this.jobName = jobName;
            this.client = client;
            this.resourcesGroupName = resourcesGroupName;
        }
        
        /**
        * Returns the serialization for the input type.
        *
        * @param userInputConfig A user's blob configuration.
        */      
        private Serialization getSerialization(JobIO userInputConfig) {
            string serializationType
                = userInputConfig.GetConfigValue("SerializationFormat");

            if (serializationType.ToLower().Equals("csv")) {
                return new CsvSerialization {
                    Properties = new CsvSerializationProperties {
                        Encoding
                        = userInputConfig.GetConfigValue("Encoding"),
                        FieldDelimiter
                        = userInputConfig.GetConfigValue("FieldDelimiter"),
                    },
                };
            } else if (serializationType.ToLower().Equals("json")) {
                return new JsonSerialization {
                    Properties = new JsonSerializationProperties {
                        Encoding = userInputConfig.GetConfigValue("Encoding")
                    },
                };
            } else if (serializationType.ToLower().Equals("avro")) {
                return new AvroSerialization();
            } else {
                throw new ConfigureJobException(
                    String.Format(
                        "Unsupported serialization {0} for input {1}",
                        serializationType,
                        userInputConfig.Name        
                    )
                );
            }
        }

        /**
        * Creates a Blob input.
        *
        * @param userInputConfig A user's blob configuration.
        * @return Blob input.
        */
        private Input CreateBlobInput(InputConfig userInputConfig) {
            Input newInput = new Input();
            newInput.Name = userInputConfig.Name;

            newInput.Properties = new StreamInputProperties() {
                Serialization = getSerialization(userInputConfig),
                DataSource = new BlobStreamInputDataSource {
                    Properties = new BlobStreamInputDataSourceProperties {
                        StorageAccounts = new StorageAccount[] {
                        new StorageAccount()
                            {
                                AccountName
                                    = userInputConfig.GetConfigValue(
                                        "AccountName"),
                                AccountKey
                                    = userInputConfig.GetConfigValue(
                                        "AccountKey")
                            }
                        },
                        Container
                            = userInputConfig.GetConfigValue("Container"),
                        PathPattern
                            = userInputConfig.GetConfigValue("PathPattern")
                    }
                }
            };

            return newInput;
        }

       /**
       * Creates an EventHub input.
       *
       * @param userInputConfig A user's event hub configuration.
       * @return Event hub input.
       */
        private Input CreateEventHubInput(InputConfig userInputConfig) {
            Input newInput = new Input();
            newInput.Name = userInputConfig.Name;

            newInput.Properties = new StreamInputProperties() {
                Serialization = getSerialization(userInputConfig),
                DataSource = new EventHubStreamInputDataSource() {
                    Properties = new EventHubStreamInputDataSourceProperties() {
                        ConsumerGroupName
                            = userInputConfig.GetConfigValue(
                                "ConsumerGroupName"),
                        EventHubName
                            = userInputConfig.GetConfigValue("EventHubName"),
                        ServiceBusNamespace
                            = userInputConfig.GetConfigValue(
                                "ServiceBusNamespace"),
                        SharedAccessPolicyKey
                            = userInputConfig.GetConfigValue(
                                "SharedAccessPolicyKey"),
                        SharedAccessPolicyName
                            = userInputConfig.GetConfigValue(
                                "SharedAccessPolicyName")
                    }
                }
            };

            return newInput;
        }

        /**
        * Creates an IotHub input.
        *
        * @param userInputConfig A user's event hub configuration.
        * @return IoT hub input.
        */
        private Input CreateIotHubInput(InputConfig userInputConfig) {
            Input newInput = new Input();
            newInput.Name = userInputConfig.Name;

            newInput.Properties = new StreamInputProperties() {
                Serialization = getSerialization(userInputConfig),
                DataSource = new IoTHubStreamInputDataSource() {
                    Properties = new IoTHubStreamInputDataSourceProperties() {
                        ConsumerGroupName
                            = userInputConfig.GetConfigValue(
                                "ConsumerGroupName"),
                        IotHubNamespace
                            = userInputConfig.GetConfigValue(
                                "IotHubNamespace"),
                        SharedAccessPolicyKey
                            = userInputConfig.GetConfigValue(
                                "SharedAccessPolicyKey"),
                        SharedAccessPolicyName
                            = userInputConfig.GetConfigValue(
                                "SharedAccessPolicyName")
                    }
                }
            };

            return newInput;
        }

        /**
        * Creates an AzureTableOutput.
        * @param userOutputConfig A user's Azure table output configuration.
        * @return An Azure table output.
        */
        public Output CreateAzureTableOutput(OutputConfig userOutputConfig) {
            Output newOutput = new Output();
            newOutput.Name = userOutputConfig.Name;
            
            newOutput.Properties = new OutputProperties() {                
                DataSource = new AzureTableOutputDataSource() {
                    Properties = new AzureTableOutputDataSourceProperties() {
                        AccountKey
                            = userOutputConfig.GetConfigValue("AccountKey"),
                        AccountName
                            = userOutputConfig.GetConfigValue("AccountName"),
                        BatchSize
                            = int.Parse(userOutputConfig
                                 .GetConfigValue("BatchSize")),
                        ColumnsToRemove
                            = userOutputConfig.GetConfigValue("ColumnsToRemove", 
                                                              false)
                                              .Split(','),
                        PartitionKey
                            = userOutputConfig.GetConfigValue("PartitionKey"),
                        RowKey = userOutputConfig.GetConfigValue("RowKey"),
                        Table = userOutputConfig.GetConfigValue("Table")
                    }
                }
            };

            return newOutput;
        }

        /**
         * Creates a Blob storage output.
         * 
         * @param userOutputConfig A user's blob storage output configuration.
         * @return An blo storage output.
         */
        public Output CreateBlobStorageOutput(OutputConfig userOutputConfig) {
            Output newOutput = new Output();
            newOutput.Name = userOutputConfig.Name;

            newOutput.Properties = new OutputProperties() {
                Serialization = getSerialization(userOutputConfig),
                DataSource = new BlobOutputDataSource() {
                    Properties = new BlobOutputDataSourceProperties() {
                        StorageAccounts = new StorageAccount[] {
                        new StorageAccount()
                            {
                                AccountName
                                    = userOutputConfig.GetConfigValue(
                                        "AccountName"),
                                AccountKey
                                    = userOutputConfig.GetConfigValue(
                                        "AccountKey")
                            }
                        },
                        Container
                            = userOutputConfig.GetConfigValue("Container"),
                        PathPattern
                            = userOutputConfig.GetConfigValue("PathPattern")
                    }
                }
            };

            return newOutput;
        }

        /**
         * Creates a SQL database output.
         * 
         * @param userOutputConfig A user's SQL output configuration.
         * @return An SQL database output.
         */
        public Output CreateSQLDatabaseOutput(OutputConfig userOutputConfig) {
            Output newOutput = new Output();
            newOutput.Name = userOutputConfig.Name;
            
            newOutput.Properties = new OutputProperties() {                
                DataSource = new SqlAzureOutputDataSource() {
                    Properties = new SqlAzureOutputDataSourceProperties() {
                       Database = userOutputConfig.GetConfigValue("Database"),
                       Password = userOutputConfig.GetConfigValue("Password"),
                       Server = userOutputConfig.GetConfigValue("Server"),
                       Table = userOutputConfig.GetConfigValue("Table"),
                       User = userOutputConfig.GetConfigValue("User")
                    }
                }
            };

            return newOutput;
        }

        /**
         * Creates a service bus queue output.
         * 
         * @param userOutputConfig A user's service bus queue configuration.
         * @return An server bus queue output.
         */
        public Output CreateServiceBusQueueOutput(
            OutputConfig userOutputConfig) {
            Output newOutput = new Output();
            newOutput.Name = userOutputConfig.Name;

            newOutput.Properties = new OutputProperties() {
                Serialization = getSerialization(userOutputConfig),
                DataSource = new ServiceBusQueueOutputDataSource() {
                    Properties = new ServiceBusQueueOutputDataSourceProperties() {
                        ServiceBusNamespace
                            = userOutputConfig.GetConfigValue(
                                "ServiceBusNamespace"),
                        QueueName
                            = userOutputConfig.GetConfigValue("QueueName"),
                        SharedAccessPolicyKey
                            = userOutputConfig.GetConfigValue(
                                "SharedAccessPolicyKey"),
                        SharedAccessPolicyName
                            = userOutputConfig.GetConfigValue(
                                "SharedAccessPolicyName")
                    }
                }
            };

            return newOutput;
        }

        /**
         * Creates a service bus topic output.
         * 
         * @param userOutputConfig A user's service bus topic configuration.
         * @return An server bus topic output.
         */
        public Output CreateServiceBusTopicOutput(
            OutputConfig userOutputConfig) {
            Output newOutput = new Output();
            newOutput.Name = userOutputConfig.Name;

            newOutput.Properties = new OutputProperties() {
                Serialization = getSerialization(userOutputConfig),
                DataSource = new ServiceBusTopicOutputDataSource() {
                    Properties
                        = new ServiceBusTopicOutputDataSourceProperties() {
                        ServiceBusNamespace
                            = userOutputConfig.GetConfigValue(
                                "ServiceBusNamespace"),
                        TopicName
                            = userOutputConfig.GetConfigValue("TopicName"),
                        SharedAccessPolicyKey
                            = userOutputConfig.GetConfigValue(
                                "SharedAccessPolicyKey"),
                        SharedAccessPolicyName
                            = userOutputConfig.GetConfigValue(
                                "SharedAccessPolicyName")
                    }
                }
            };

            return newOutput;
        }

        /**
         * Creates a DocumentDB output.
         * 
         * @param userOutputConfig A user's DocumentDB configuration.
         * @return An DocumentDB output.
         */
        public Output CreateDocumentDBOutput(
            OutputConfig userOutputConfig) {
            Output newOutput = new Output();
            newOutput.Name = userOutputConfig.Name;

            newOutput.Properties = new OutputProperties() {
                DataSource = new DocumentDbOutputDataSource() {
                    Properties
                        = new DocumentDbOutputDataSourceProperties() {
                            AccountKey
                                = userOutputConfig.GetConfigValue("AccountKey"),
                            AccountId
                                = userOutputConfig.GetConfigValue("AccountId"),
                            CollectionNamePattern
                                = userOutputConfig.GetConfigValue("CollectionNamePattern"),
                            Database
                                = userOutputConfig.GetConfigValue("Database"),
                            DocumentId
                                = userOutputConfig.GetConfigValue("DocumentId", 
                                                                  false),
                            PartitionKey
                                = userOutputConfig.GetConfigValue("PartitionKey")
                        }
                }
            };

            return newOutput;
        }

        /**
        * Adds an input to the job.
        */
        public void addInput(InputConfig userInputConfig) {
            // validate input name and type
            if (userInputConfig.Name == null
             || userInputConfig.Name.Length == 0) {
                throw new ConfigureJobException(
                    "Input name must be a non-empty string");
            }

            if (userInputConfig.Type == null
             || userInputConfig.Type.Length == 0) {
                throw new ConfigureJobException(
                    "Input type must be a non-empty string");
            }

            // Create a Stream Analytics input source
            Input newInput = null;

            // check the type of the input to set serialization
            if (userInputConfig.Type.ToLower().Equals("blob")) {
                newInput = CreateBlobInput(userInputConfig);
            } else if (userInputConfig.Type.ToLower().Equals("eventhub")) {
                newInput = CreateEventHubInput(userInputConfig);
            } else if (userInputConfig.Type.ToLower().Equals("iothub")) {
                newInput = CreateIotHubInput(userInputConfig);
            } else {
                throw new ConfigureJobException(
                    "Unsupported job input type: " + userInputConfig.Type);
            }

            InputCreateOrUpdateParameters jobInputCreateParameters
                = new InputCreateOrUpdateParameters() {
                    Input = newInput
                };

            InputCreateOrUpdateResponse inputCreateResponse =
                client.Inputs.CreateOrUpdate(resourcesGroupName,
                                             jobName,
                                             jobInputCreateParameters);

            DPClient.validateStatusCode(inputCreateResponse.StatusCode);
        }

        /**
        * Adds an output to the job.
        */
        public void addOutput(OutputConfig userOutputConfig) {
            // validate input name and type
            if (userOutputConfig.Name == null
             || userOutputConfig.Name.Length == 0) {
                throw new ConfigureJobException(
                    "Output name must be a non-empty string");
            }

            if (userOutputConfig.Type == null
             || userOutputConfig.Type.Length == 0) {
                throw new ConfigureJobException(
                    "Output type must be a non-empty string");
            }

            // Create a Stream Analytics input source
            Output newOutput = null;

            // check the type of the input to set serialization
            if (userOutputConfig.Type.ToLower().Equals("blob")) {
                newOutput = CreateBlobStorageOutput(userOutputConfig);
            } else if (userOutputConfig.Type.ToLower().Equals("azuretable")) {
                newOutput = CreateAzureTableOutput(userOutputConfig);
            } else if (userOutputConfig.Type.ToLower().Equals("sql")) {
                newOutput = CreateSQLDatabaseOutput(userOutputConfig);
            } else if (userOutputConfig.Type.ToLower().Equals("servicebusqueue")) {
                newOutput = CreateServiceBusQueueOutput(userOutputConfig);
            } else if (userOutputConfig.Type.ToLower().Equals("servicebustopic")) {
                newOutput = CreateServiceBusTopicOutput(userOutputConfig);
            } else if (userOutputConfig.Type.ToLower().Equals("documentdb")) {
                newOutput = CreateDocumentDBOutput(userOutputConfig);
            } else {
                throw new ConfigureJobException(
                    "Unsupported job output type: "
                  + userOutputConfig.Type);
            }

            OutputCreateOrUpdateParameters jobOutputCreateParameters
                = new OutputCreateOrUpdateParameters() {
                    Output = newOutput
                };

            OutputCreateOrUpdateResponse inputCreateResponse =
                client.Outputs.CreateOrUpdate(resourcesGroupName,
                                             jobName,
                                             jobOutputCreateParameters);

            DPClient.validateStatusCode(inputCreateResponse.StatusCode);
        }

        /**
        * Sets the transform for a job.
        *
        * @param transform The transform as a string.
        */
        public void setTransform(string transform, int? streamingUnits) {
            TransformationCreateOrUpdateParameters parameters =
                new TransformationCreateOrUpdateParameters() {
                    Transformation = new Transformation() {
                        Name = DEFAULT_TRANSFORM_NAME,
                        Properties = new TransformationProperties() {
                            StreamingUnits = streamingUnits,
                            Query = transform
                        }                        
                    }
                };

            /**
            * NOTE: We do a try/catch here to workaround a limitation with
            * current API. The CreateOrUpdate function only creates, it
            * does not update, a tranformation. If it fails, we fall back
            * on the Patch function.
            */
            try {
                TransformationCreateOrUpdateResponse response
                    = client.Transformations.CreateOrUpdate(
                        resourcesGroupName,
                        jobName,
                        parameters);

                DPClient.validateStatusCode(response.StatusCode);
            } catch (Exception e) {
                TransformationPatchResponse response
                    = client.Transformations.Patch(
                        resourcesGroupName,
                        jobName,
                        DEFAULT_TRANSFORM_NAME,
                        new TransformationPatchParameters() {
                            Properties = parameters.Transformation.Properties
                        }
                    );

                DPClient.validateStatusCode(response.StatusCode);
            }
        }

        /**
        * Returns the transform for the job.
        */
        public Tuple<string, int?> getTransform() {
            JobGetResponse jobResponse = client.StreamingJobs.Get(
                resourcesGroupName,
                jobName,
                new JobGetParameters() {
                    PropertiesToExpand = "transformation"
                }
            );

            DPClient.validateStatusCode(jobResponse.StatusCode);

            if (jobResponse.Job.Properties.Transformation == null) {
                return Tuple.Create<string, int?>("", 1);
            } else {
                var prop = jobResponse.Job
                                      .Properties
                                      .Transformation
                                      .Properties;

                return Tuple.Create(prop.Query, 
                                    prop.StreamingUnits);
            }               
        }

        /**
        * Adds a funciton to a job.
        */
        public void addFunction(JobFunction func) {
            FunctionBinding funcBinding = null;

            FunctionCreateOrUpdateParameters parameters
                = new FunctionCreateOrUpdateParameters() {
                    Function = new Function() {
                        Name = func.Name,
                        Properties = new ScalarFunctionProperties() {
                            Properties = new ScalarFunctionConfiguration() {}
                        }
                    }
                };

            if (func.Type.ToLower().Equals("javascript")) {
                funcBinding = new JavaScriptFunctionBinding() {                               
                    Properties = new JavaScriptFunctionBindingProperties() {
                        Script = func.GetConfigValue("Script")                                                
                    }
                };

                // set output type
                ((ScalarFunctionProperties)parameters.Function.Properties)
                .Properties.Output = new FunctionOutput() {
                    DataType = func.GetConfigValue("Output")
                };

                // set input types
                ((ScalarFunctionProperties)parameters.Function.Properties)
                .Properties.Inputs = new List<FunctionInput>();

                foreach (string inputType 
                         in func.GetConfigValue("Inputs").Split(',')) {

                    ((ScalarFunctionProperties)parameters.Function.Properties)
                    .Properties.Inputs.Add(new FunctionInput() {
                        DataType = inputType
                    });
                }

            } else if (func.Type.ToLower().Equals("ml"))  {
                funcBinding
                    = new AzureMachineLearningWebServiceFunctionBinding() {
                    Properties
                        = new AzureMachineLearningWebServiceFunctionBindingProperties() {
                        ApiKey = func.GetConfigValue("ApiKey"),
                        Endpoint = func.GetConfigValue("Endpoint"),
                        BatchSize = int.Parse(func.GetConfigValue("BatchSize"))                                
                    }
                };
            } else {
                throw new ConfigureJobException(
                    "Unsupported function type: " + func.Type
                );
            }

            // set function binding
            ((ScalarFunctionProperties) parameters.Function.Properties)
                .Properties.Binding = funcBinding;
            
            FunctionCreateOrUpdateResponse response
                = client.Functions.CreateOrUpdate(resourcesGroupName, 
                                                  jobName, 
                                                  parameters);

            DPClient.validateStatusCode(response.StatusCode);
        }

        /**
        * Resets the configuration of the job.
        */
        public void reset() {
            // remove all inputs and outputs
            InputListResponse inputResponse
                = client.Inputs.ListInputInJob(resourcesGroupName, 
                                              jobName, 
                                              new InputListParameters());

            foreach (Input currInput in inputResponse.Value) {
                client.Inputs.Delete(resourcesGroupName, 
                                     jobName, 
                                     currInput.Name);
            }

            OutputListResponse outputResponse
             = client.Outputs.ListOutputInJob(resourcesGroupName,
                                              jobName,
                                              new OutputListParameters());

            foreach (Output currOutput in outputResponse.Value) {
                client.Outputs.Delete(resourcesGroupName,
                                      jobName,
                                      currOutput.Name);
            }

            // We don't have the option to delete a tranform, therefore we
            // just reset the query string and streaming units. We use a single
            // space for the query otherwise it appears as missing in the
            // request.
            setTransform(" ", 1);

            // reset functions
            FunctionListResponse functionResponse
                = client.Functions.ListFunctionsInJob(resourcesGroupName,
                                                      jobName);

            foreach (Function func in functionResponse.Value) {
                client.Functions.Delete(resourcesGroupName, 
                                        jobName, 
                                        func.Name);
            }
        }

        /**
        * Configures a job based on properties from a config file. 
        */
        public static JobConfig Config(StreamAnalyticsManagementClient client,
                                       string resourcesGroupName,
                                       string jobName, 
                                       string configFilePath,
                                       Boolean isResetConfigRequested=false,
                                       Boolean isForceRequested=false) {          
            // read in the config file and parse it as a dictionary
            JobConfig config
                = JsonConvert.DeserializeObject<JobConfig>(
                    File.ReadAllText(configFilePath));

            ConfigureJob jobConfigurer = new ConfigureJob(jobName, 
                                                          client,
                                                          resourcesGroupName);

            if (isForceRequested) {
                // check if the job is runnning
                JobGetResponse jobResponse
                    = client.StreamingJobs.Get(resourcesGroupName, 
                                               jobName, 
                                               new JobGetParameters());

                if (jobResponse.Job.Properties.JobState.Equals("Running")) {
                    Console.WriteLine("Stopping the job (this could take awhile)");

                    LongRunningOperationResponse response
                        = client.StreamingJobs.Stop(resourcesGroupName,
                                                    jobName);

                    if (response.StatusCode != System.Net.HttpStatusCode.OK) {
                        throw new ConfigureJobException(
                            "Unable to stop the job before updating its configuration"
                        );
                    }

                    Console.WriteLine("Job stopped");
                }
            }

            if (isResetConfigRequested) {
                // reset existing configuration
                jobConfigurer.reset();
            }

            // add inputs and outputs
            foreach (InputConfig userInputConfig in config.Inputs) {
                jobConfigurer.addInput(userInputConfig);
            }

            foreach (OutputConfig userOutputConfig in config.Outputs) {
                jobConfigurer.addOutput(userOutputConfig);
            }

            if (config.Transform != null) {
                if (!config.Transform.ContainsKey("Query")) {
                    throw new ConfigureJobException(
                        "Missing Transform configuration setting 'Query'"
                    );
                }

                if (!config.Transform.ContainsKey("StreamingUnits")) {
                    throw new ConfigureJobException(
                        "Missing Transform configuration setting 'StreamingUnits'"
                    );
                }

                jobConfigurer.setTransform(
                    config.Transform["Query"], 
                    int.Parse(config.Transform["StreamingUnits"]));
            }

            // add functions
            foreach (JobFunction func in config.Functions) {
                jobConfigurer.addFunction(func);
            }

            return config;
        }
    }
}
