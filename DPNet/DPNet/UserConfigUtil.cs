﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DPNet {
    /**
    * User configuration for the dymamic data processing API (stored
    * externally in a JSON file).
    */
    class UserConfig {
        // account configuration
        public string ActiveDirectoryTenantId { get; set; }
        public string SubscriptionId { get; set; }
        public string ResourcesGroupName { get; set; }
        public string DefaultLocation { get; set; }
        public string ServiceBusNamespace { get; set; }
        public string SharedAccessPolicyName { get; set; }
        public string SharedAccessPolicyKey { get; set; }

        // authentication information
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
    }
}
