﻿using System;
using System.Configuration;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text;

using Microsoft.Azure;
using Microsoft.Azure.Management.StreamAnalytics;
using Microsoft.Azure.Management.StreamAnalytics.Models;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using Microsoft.ServiceBus.Messaging;
using Microsoft.ServiceBus;
using JobConfigUtil;

/**
* Utilities for managing StreamAnalytics jobs in the cloud.
*/
namespace DPNet {
    /**
    * Contains API for dynamically making changes to StreamAnalytics jobs.
    */
    class DPClient {
        private StreamAnalyticsManagementClient client;
        private UserConfig userConfig;

        /**
        * Constructs a new DPClient.
        *
        * @param client Manages StreamAnalytics jobs.
        * @param userConfig User configuration for Azure.
        */
        public DPClient(StreamAnalyticsManagementClient client, 
                        UserConfig userConfig) {
            this.client = client;
            this.userConfig = userConfig;
        }
        
        /**
        * Validates that the status code is an OK message (or another type
        * of message).
        * @param c An status code.
        * @param expected The expected status code.
        */
        public static void validateStatusCode(
            HttpStatusCode c,
            HttpStatusCode expected = HttpStatusCode.OK) {
            if (c != expected) {
                throw new Exception(
                    String.Format(
                        "Expected OK status code, got {0} instead", c));
            }
        }

        /**
        * Represents an EventHub connection between two jobs.
        */
        class EventHubConnector {
            public string eventHubName { get; set; }
            public string serviceBusNamespace { get; set; }

            public override bool Equals(Object obj) {
                if (obj == null || GetType() != obj.GetType()) {
                    return false;
                }

                EventHubConnector other = (EventHubConnector)obj;
                return (eventHubName == other.eventHubName)
                    && (serviceBusNamespace == other.serviceBusNamespace);
            }

            public override int GetHashCode() {
                return eventHubName.GetHashCode()
                     ^ serviceBusNamespace.GetHashCode();
            }
        }

        /**
        * Returns EventHub outputs for the job (i.e. edges in the DP graph).
        */
        private List<EventHubConnector> getEventHubOutputs(Job job) {
            List<EventHubConnector> outputEventHubs
                = new List<EventHubConnector>();

            foreach (var output in job.Properties.Outputs) {
                // check if this is an EventHub output
                if (output.Properties
                          .DataSource
                          .Type
                          .Equals("Microsoft.ServiceBus/EventHub")) {
                    // found an input/output match
                    var dataSource
                        = (EventHubOutputDataSource)output.Properties.DataSource;
                    outputEventHubs.Add(
                        new EventHubConnector() {
                            eventHubName
                                = dataSource.Properties.EventHubName,
                            serviceBusNamespace
                                = dataSource.Properties.ServiceBusNamespace
                        }
                    );
                }
            }

            return outputEventHubs;
        }

        /**
        * Gets EventHub inputs for the job (i.e. edges in the DP graph).
        */
        private List<EventHubConnector> getEventHubInputs(Job job) {
            List<EventHubConnector> inputEventHubs
                = new List<EventHubConnector>();

            foreach (var input in job.Properties.Inputs) {
                StreamInputProperties prop =
                    (StreamInputProperties)input.Properties;

                // check if this is an EventHub input
                if (prop.DataSource
                        .Type
                        .Equals("Microsoft.ServiceBus/EventHub")) {
                    // found an input/output match
                    var dataSource = (EventHubStreamInputDataSource)prop.DataSource;
                    inputEventHubs.Add(
                        new EventHubConnector() {
                            eventHubName
                                = dataSource.Properties.EventHubName,
                            serviceBusNamespace
                                = dataSource.Properties.ServiceBusNamespace
                        }
                    );
                }
            }

            return inputEventHubs;
        }

        /**
        * Returns a list of downstream jobs (i.e. adjacent nodes in the DP
        * graph).
        */
        private List<Tuple<Job, EventHubConnector>> getDownstreamJobs(
            Job job, List<Job> allJobs) {
            List<Tuple<Job, EventHubConnector>> downstreamJobs
                = new List<Tuple<Job, EventHubConnector>>();

            List<EventHubConnector> outputEventHubs = getEventHubOutputs(job);

            // find downstream (i.e. adjacent) jobs
            foreach (var currJob in allJobs) {
                if (currJob.Name != job.Name) {
                    List<EventHubConnector> inputEventHubs
                        = getEventHubInputs(currJob);

                    foreach (var inputEventHub in inputEventHubs) {
                        // Check if the input to the other job matches an
                        // output for this job
                        if (outputEventHubs.Contains(inputEventHub)) {
                            downstreamJobs.Add(
                                Tuple.Create(currJob, inputEventHub));
                        }
                    }
                }
            }

            return downstreamJobs;
        }

        /**
        * Finds the graph of all downstream jobs.
        */
        private void findJobGraph(Job job,
                                  List<Job> allJobs,
                                  HashSet<string> visitedJobNames) {
            visitedJobNames.Add(job.Name);

            foreach (var jobTuple in getDownstreamJobs(job, allJobs)) {
                var currJob = jobTuple.Item1;
                var eventHubConnector = jobTuple.Item2;

                Console.WriteLine(
                    String.Format(
                        "{0} -> {1}, Service Bus: {2}, EventHub: {3}",
                        job.Name,
                        currJob.Name,
                        eventHubConnector.serviceBusNamespace,
                        eventHubConnector.eventHubName
                    )
                );

                // find other jobs
                if (visitedJobNames.Contains(currJob.Name)) {
                    Console.WriteLine(
                        String.Format(
                            "Cycle detected, {0} is a job that's "
                          + "already been encountered in the graph",
                            currJob.Name
                        )
                    );
                } else {
                    visitedJobNames.Add(currJob.Name);
                    findJobGraph(currJob, allJobs, visitedJobNames);
                }
            }
        }       

        /**
        * Displays the DP graph associated with a StreamAnalytics job.
        *
        * @param jobName The name of the StreamAnalytics job.
        * @param resourceGroupName The name of the resources group that the
        * job is a part of.
        * @param includeAllJobs If true, lists all jobs and their connectivity
        * (and ignores jobName), otherwise only includes the specified job.
        */
        public void viewDPGraph(string jobName,
                                string resourceGroupName,
                                Boolean includeAllJobs) {
            // Get a list of all jobs
            JobListResponse jobList
                = client.StreamingJobs.ListJobsInResourceGroup(
                    resourceGroupName,
                    new JobListParameters() {
                        PropertiesToExpand = "inputs,outputs"
                    }
            );

            // extract all inputs for other jobs (to check if the output
            // of this job connects to the input of other jobs)            
            List<Job> allJobs = new List<Job>();
            Job chosenJob = null;

            for (int i = 0; i < jobList.Value.Count; i++) {
                var currJob = jobList.Value[i];
                allJobs.Add(currJob);

                if (currJob.Name.Equals(jobName)) {
                    chosenJob = currJob;
                }
            }

            // sort based on job names (for consistent iteration0
            allJobs.Sort((x, y) => x.Name.CompareTo(y.Name));

            // check if the job was found
            if (chosenJob == null && !includeAllJobs) {
                Console.WriteLine(
                    String.Format(
                        "'{0}' could not be found, please check the job "
                      + "name and the account configuration",
                        jobName));
                Environment.Exit(1);
            }

            // find connected jobs
            if (includeAllJobs) {
                foreach (Job currJob in allJobs) {
                    Console.WriteLine(
                        String.Format("{0} job graph:", currJob.Name));
                    findJobGraph(currJob, allJobs, new HashSet<string>());

                    // create blank line
                    Console.WriteLine();
                }
            } else {
                Console.WriteLine(
                    String.Format("{0} job graph:", chosenJob.Name));
                findJobGraph(chosenJob, allJobs, new HashSet<string>());
            }
        }


        /**
        * Removes the EventHub that connects two jobs.
        * 
        * @param srcJob Name of the source job.
        * @param destJob Name of the destination job.
        * @param srcJobOutputName Name of the output of the source job.
        * @param destJobInputName Name of the input of the destination job.
        * @param resourceGroupName The name of the resources group that the
        * jobs and EventHub are a part of.
        */
        public void disconnectJobs(string srcJobName,
                                   string destJobName,
                                   string srcJobOutputName,
                                   string destJobInputName,
                                   string resourceGroupName) {
            // delete the output from the source job
            CommonOperationResponse srcJobResponse
                = client.Outputs.Delete(resourceGroupName,
                                        srcJobName,
                                        srcJobOutputName);

            validateStatusCode(srcJobResponse.StatusCode);

            // delete the input from the destination job
            CommonOperationResponse destJobResponse
                = client.Outputs.Delete(resourceGroupName,
                                        destJobName,
                                        destJobInputName);

            validateStatusCode(destJobResponse.StatusCode,
                               HttpStatusCode.NoContent);
        }

        /**
        * Checks if two jobs are connected by an EventHub.
        * 
        * @param srcJobName Name of the source job.
        * @param destJobName Name of the destination job.
        * @param resourceGroupName The resource group.
        *
        * @return Returns true if the jobs are connected by two jobs, else 
        * false.
        */
        public Boolean isConnected(string srcJobName, 
                                   string destJobName,
                                   string resourceGroupName) {
            List<Job> allJobs = getAllJobs(resourceGroupName);

            Job srcJob = allJobs.Find(x => x.Name.Equals(srcJobName));

            if (srcJob == null) {
                throw new Exception(
                    String.Format("{0} is not a valid job", srcJobName)
                );
            }

            // check if the destination job is among the downstream jobs
            List<Tuple<Job, EventHubConnector>> downstreamJobs
                = getDownstreamJobs(srcJob, allJobs);

            return downstreamJobs.Find(x => x.Item1.Name.Equals(destJobName)) 
                   != null;
        }

        /**
        * Connects two StreamAnalytics jobs (creates an EventHub to connect
        * the input and outputs), creating an EventHub for the jobs
        * one does not already exist.
        * 
        * @param srcJobName Name of the source job.
        * @param destJobName Name of the destination job.
        * @param serviceBusNamespace Namespace for the EventHub.
        * @param resourceGroupName The name of the resources group that the
        * jobs and EventHub are a part of.
        * @param sharedAccessPolicyName The name of the shared access policy
        * for the EventHub configuration.
        * @param sharedAccessPolicyKey The name of the shared access policy
        * key for the EventHub configuration.
        *
        * @return A tuple with the EventHub output name for the source job
        * and the EventHub input name of the destination job, null if
        * both jobs are already connected by an EventHub.
        */
        public Tuple<string, string> connectJobs(string srcJobName,
                                                 string destJobName,
                                                 string serviceBusNamespace,
                                                 string resourceGroupName,
                                                 string sharedAccessPolicyName,
                                                 string sharedAccessPolicyKey) {
            if (isConnected(srcJobName, destJobName, resourceGroupName)) {
                return null;
            }

            // NOTE: The code below follows examples from the link below
            // for creating an EventHub and doing authentication for it:
            // https://docs.microsoft.com/en-us/azure/event-hubs/event-hubs-authentication-and-security-model-overview

            // get the service bus manager
            string serviceBusPath = String.Format("sb://{0}.servicebus.windows.net",
                                                  serviceBusNamespace);
            
            // create EventHub name
            string eventHubName = String.Format("EventHubConnector_{0}_to_{1}",
                                                srcJobName,
                                                destJobName);

            // Create namespace manager.
            Uri uri = ServiceBusEnvironment.CreateServiceUri(
                "sb", 
                serviceBusNamespace, 
                string.Empty);

            TokenProvider td
                = TokenProvider.CreateSharedAccessSignatureTokenProvider(
                    sharedAccessPolicyName, 
                    sharedAccessPolicyKey);

            NamespaceManager nm = new NamespaceManager(uri, td);

            // Create event hub with a SAS rule that enables sending to that event hub
            EventHubDescription ed = new EventHubDescription(eventHubName) {
                PartitionCount = 2
            };

            string eventHubSendKeyName = "EventHubSendKey";
            string eventHubSendKey
                = SharedAccessAuthorizationRule.GenerateRandomKey();

            SharedAccessAuthorizationRule eventHubSendRule
                = new SharedAccessAuthorizationRule(
                    eventHubSendKeyName, 
                    eventHubSendKey, 
                    new[] { AccessRights.Send });

            ed.Authorization.Add(eventHubSendRule);
            nm.CreateEventHubIfNotExists(ed);

            return connectJobs(srcJobName, 
                               destJobName, 
                               eventHubName, 
                               serviceBusNamespace,
                               resourceGroupName,
                               sharedAccessPolicyName,
                               sharedAccessPolicyKey);
        }

        /**
        * Connects two StreamAnalytics jobs via an EventHub.
        * 
        * @param srcJob Name of the source job.
        * @param destJob Name of the destination job.
        * @param eventHubName Name of the EvetHub to connect jobs by.
        * @param serviceBusNamespace Namespace for the EventHub.
        * @param resourceGroupName The name of the resources group that the
        * jobs and EventHub are a part of.
        * @param sharedAccessPolicyName The name of the shared access policy
        * for the EventHub configuration.
        * @param sharedAccessPolicyKey The name of the shared access policy
        * key for the EventHub configuration.
        *
        * @return A tuple with the EventHub output name for the source job
        * and the EventHub input name of the destination job.
        */
        public Tuple<string, string> connectJobs(string srcJobName,
                                                 string destJobName,
                                                 string eventHubName,
                                                 string serviceBusNamespace,
                                                 string resourceGroupName,
                                                 string sharedAccessPolicyName,
                                                 string sharedAccessPolicyKey) {
            // check if the jobs are already connected
            var eventHubConnectorNames
                = Tuple.Create(destJobName + "Output", srcJobName + "Input");

            // error checking
            if (srcJobName.Equals(destJobName)) {
                throw new Exception(
                    String.Format(
                        "Source job {0} matches destination job {1}",
                        srcJobName,
                        destJobName
                    )
                );
            }

            // define the serialization that the event hub will use
            JsonSerialization eventHubSer = new JsonSerialization {
                Properties = new JsonSerializationProperties {
                    Encoding = "UTF8"
                }
            };

            OutputCreateOrUpdateParameters jobOutputCreateParameters
                = new OutputCreateOrUpdateParameters() {
                    Output = new Output() {
                        Name = eventHubConnectorNames.Item1,
                        Properties = new OutputProperties() {
                            DataSource = new EventHubOutputDataSource() {
                                Properties = new EventHubOutputDataSourceProperties() {
                                    EventHubName = eventHubName,
                                    ServiceBusNamespace = serviceBusNamespace,
                                    SharedAccessPolicyName = sharedAccessPolicyName,
                                    SharedAccessPolicyKey = sharedAccessPolicyKey
                                }
                            },
                            Serialization = eventHubSer
                        }
                    }
                };

            // make request to update output
            OutputCreateOrUpdateResponse outputCreateResponse =
                client.Outputs.CreateOrUpdate(
                    resourceGroupName,
                    srcJobName,
                    jobOutputCreateParameters);

            validateStatusCode(outputCreateResponse.StatusCode);

            InputCreateOrUpdateParameters jobInputCreateParameters                
                = new InputCreateOrUpdateParameters() {
                Input = new Input() {
                    Name = eventHubConnectorNames.Item2,
                    Properties = new StreamInputProperties() {
                        DataSource = new EventHubStreamInputDataSource() {
                            Properties = new EventHubStreamInputDataSourceProperties() {
                                EventHubName = eventHubName,
                                ServiceBusNamespace = serviceBusNamespace,
                                SharedAccessPolicyName = sharedAccessPolicyName,
                                SharedAccessPolicyKey = sharedAccessPolicyKey
                            }
                        },
                        Serialization = eventHubSer
                    }
                }
            };

            InputCreateOrUpdateResponse inputCreateResponse =
                client.Inputs.CreateOrUpdate(
                    resourceGroupName,
                    destJobName,
                    jobInputCreateParameters);

            validateStatusCode(outputCreateResponse.StatusCode);

            // update the source job's transformation to include the
            // new EventHub output
            ConfigureJob configurer = new ConfigureJob(
                srcJobName, 
                client, 
                resourceGroupName);

            // update the transform to include a new template SQL string
            // for the new EventHub update
            Tuple<string, int?> transform = configurer.getTransform();

            string newTransformQuery
                = transform.Item1
                + "\n"
                + "/* "
                + String.Format(
                    "select * into {0} from <DATA_SOURCE>",
                    eventHubConnectorNames.Item1)
                + " */";

            configurer.setTransform(newTransformQuery, transform.Item2);

            return eventHubConnectorNames;
        }

        /**
        * Convenience method for creating multiple jobs (only creates each
        * job if it doesn't already exist).
        */
        public void createJobs(string[] jobNames,
                               string resourceGroupName,
                               string location) {
            List<Job> allJobs = getAllJobs(resourceGroupName);

            HashSet<string> allJobNames
                = new HashSet<string>(allJobs.ConvertAll<string>(x => x.Name));

            foreach (string jobName in jobNames) {
                if (!allJobNames.Contains(jobName)) {
                    createJob(jobName, resourceGroupName, location);
                }
            }
        }

        /**
        * Creates a new job.
        * 
        * @param jobName The name of the job to create.
        * @param subscriptionId The subscription ID for the job.
        * @param resourceGroupName The resource group the job will belong to.
        * @param location The location where the job should run.
        */
        public void createJob(string jobName,
                              string resourceGroupName,
                              string location) {
            JobCreateOrUpdateParameters jobCreateParameters
                = new JobCreateOrUpdateParameters() {
                    Job = new Job() {
                        Name = jobName,
                        Location = location,
                        Properties = new JobProperties() {
                            EventsOutOfOrderPolicy = EventsOutOfOrderPolicy.Adjust,
                            Sku = new Sku() {
                                Name = "Standard"
                            }
                        }
                    }
                };

            JobCreateOrUpdateResponse response
                = client.StreamingJobs.CreateOrUpdate(resourceGroupName,
                                                      jobCreateParameters);

            validateStatusCode(response.StatusCode);
        }

        /**
        * Returns all jobs (for the given user configuration).
        *
        * @param resourceGroupName The resource group.
        */
        public List<Job> getAllJobs(string resourceGroupName) {
            // Get a list of all jobs
            JobListResponse jobList
                = client.StreamingJobs.ListJobsInResourceGroup(
                    resourceGroupName,
                    new JobListParameters() {
                        PropertiesToExpand = "inputs,outputs"
                    }
            );

            List<Job> allJobs = new List<Job>();

            foreach (Job currJob in jobList.Value) {
                allJobs.Add(currJob);
            }

            return allJobs;
        }

        /**
        * Starts a job (if it's not running already).
        * @param jobName The name of the job to create.
        * @param resourceGroupName The resource group the job will belong to.
        */
        public void startJob(string jobName, string resourcesGroupName) {
            // check if the job is runnning
            JobGetResponse jobResponse
                = client.StreamingJobs.Get(resourcesGroupName,
                                           jobName,
                                           new JobGetParameters());

            if (jobResponse.Job.Properties.JobState.Equals("Running")) {
                Console.WriteLine(jobName + " is already running");
            } else {
                Console.WriteLine("Starting the job: " + jobName);

                try {
                    LongRunningOperationResponse response
                        = client.StreamingJobs.Start(
                            resourcesGroupName,
                            jobName,
                            new JobStartParameters());

                    DPClient.validateStatusCode(response.StatusCode);

                    Console.WriteLine(jobName + " has started");
                } catch (Exception e) {
                    Console.Error.WriteLine(
                        "Unable to start job " + jobName + ": " + e.Message);
                }
            }
        }

        /**
        * Stops a job (if it's not running already).
        * @param jobName The name of the job to create.
        * @param resourceGroupName The resource group the job will belong to.
        */
        public void stopJob(string jobName, string resourcesGroupName) {
            // check if the job is stopped
            JobGetResponse jobResponse
                = client.StreamingJobs.Get(resourcesGroupName,
                                           jobName,
                                           new JobGetParameters());

            if (jobResponse.Job.Properties.JobState.Equals("Stopped")) {
                Console.WriteLine(jobName + " is already stopped");
            } else {
                Console.WriteLine("Stopping the job: " + jobName);

                try {
                    LongRunningOperationResponse response
                        = client.StreamingJobs.Stop(
                            resourcesGroupName, 
                            jobName);

                    DPClient.validateStatusCode(response.StatusCode);

                    Console.WriteLine(jobName + " has stopped");
                } catch (Exception e) {
                    Console.Error.WriteLine(
                        "Unable to stop job " + jobName + ": " + e.Message);
                }
            }
        }
    }
}
