# Dynamic Data Processing

This tool enables dynamic management of StreamAnalytics jobs in Azure. With a few command-line calls you can create new jobs, establish data flow via EventHubs, and view the connectivity of StreamAnalytics jobs.

## Prerequisites

To use the DPNet.exe tool and .NET project you must have an active Azure subscription. As a developer, you must have Visual Studio 2015 or 2017.

## Getting Started
The executable is found here: **dist/DPNet.exe**. In order to use it, you must configure it to access your Azure account.

### Setting Credentials in azure.json
The template credentials file can be found here: **dist/azure.json**. This file must be kept in the same folder as DPNet.exe

1. To set **ClientId** and **ClientSecret** see **[this tutorial](https://docs.microsoft.com/en-us/azure/azure-resource-manager/resource-group-create-service-principal-portal#create-an-azure-active-directory-application)** (during the tutorial, set the "Application type" to Native and the "Redirect URI" to urn:ietf:wg:oauth:2.0:oob) and then **[this tutorial](https://docs.microsoft.com/en-us/azure/azure-resource-manager/resource-group-create-service-principal-portal#get-application-id-and-authentication-key)**. 
2. To get **ActiveDirectoryTenantId ** see **[this tutorial](https://stackoverflow.com/a/41028320)**.
3. To get **SubscriptionId** see **[this tutorial](https://blogs.msdn.microsoft.com/mschray/2016/03/18/getting-your-azure-subscription-guid-new-portal/)**.
4. Set **ResourcesGroupName** to the name of the resources group for your Azure StreamAnalytics jobs. The tool only allows you to manage StreamAnalytics jobs within a single resource group.
5. To establish connections between StreamAnalytics jobs, you must have a service bus namespace 
 in your Azure account. Set **ServiceBusNamespace** to your service bus namespace.
6. To set **SharedAccessPolicyName** and **SharedAccessPolicyKey**, you must have a shared access policy defined for your service bus namespace (with the claims set to Manage, Send, and Listen). Set the policy key to the Primary Key listed for the policy.

### Usage
The application provides a command-line interface, use the `-h` or `help` options to view its usage. Examples are listed below.

#### Creating jobs
```
...\dist> DPNet.exe create job tempreader;homemonitor
```

#### Connecting jobs
```
...\dist> DPNet.exe create flow tempreader;homemonitor
```

#### Viewing jobs and their connectivity
```
...\dist> DPNet.exe view --all

homemonitor job graph:

tempreader job graph:
tempreader -> homemonitor, Service Bus: testeventhubgf, EventHub: EventHubConnector_tempreader_to_homemonitor
```

#### Setting job configuration
A simple template configuration file can be found here: **dist/environment.json**. A more complete template configuration file that demonstrates all possible inputs and outputs can be found here: **dist/full_config.json**. Both files have credentials omitted.
```
...\dist> DPNet.exe config environment.json tempreader
```